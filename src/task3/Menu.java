package task3;

import java.util.Scanner;

public class Menu {
	public static void MainMenu() {
		int choice = 999;
		int firstInt;
		int secondInt;
		double firstDouble;
		double secondDouble;
		
		String regex = "[-+]?([0-9]*\\.[0-9]+|[0-9]+)";
		String regex1 = "[+-]?\\d+(\\.?\\d+)";
		Calculator calc = new Calculator();
		
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println("To exit the proggram enter 0 for both values.");
			System.out.println("Input first value");
			String firstValue = scanner.nextLine();
			
			System.out.println("Input second value");
			String secondValue = scanner.nextLine();
			
			if(firstValue.isEmpty() || secondValue.isEmpty()) {
				System.out.println("No empty values!");
				continue;
			}
			
			if(firstValue.matches(regex1)) {
				System.out.println("first is double");
			}
			else
				System.out.println("first is not double");
			
			if(secondValue.matches(regex1)) {
				System.out.println("second is double");
			}
			else
				System.out.println("second is not double");
			
			
			System.out.println("Now choose an operation");
			System.out.println("1. +");
			System.out.println("2. -");
			System.out.println("3. *");
			System.out.println("4. /");
			
			choice = scanner.nextInt();
			
			switch(choice) {
			case 1:
				break;
			}
			
			
		} while (choice != 0);

		scanner.close();
	}
}
