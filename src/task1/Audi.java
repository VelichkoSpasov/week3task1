package task1;

/**
 * 
 * @author Velichko Spasov
 *
 */

public class Audi extends Car{
	public String model;
	public int horsePower;
	public float accelerationTime;
	
	/**
	 * 
	 * @param mod - Car's model.
	 * @param hp - Car's horse powers.
	 * @param acc - Car's acceleration.
	 */
	public Audi(String mod, int hp, float acc) {
		model = mod;
		horsePower = hp;
		accelerationTime = acc;
	}

	/**
	 * Prin all attributes of a car.
	 */
	@Override
	public String toString() {
		return "This car is " + Audi.class.getSimpleName() + " model " + model + " with " + horsePower + " horsepower "
				+ "and acceleration from 0 to 60 miles/h for " + accelerationTime + "s.";
	}
}
