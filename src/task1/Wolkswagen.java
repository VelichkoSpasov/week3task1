package task1;
/**
 * 
 * @author veichko Spasov
 *
 */
public class Wolkswagen extends Car{
	public String model;
	public int horsePower;
	public double accelerationTime;
	
	/**
	 * 
	 * @param mod - Car's model.
	 * @param hp - Car's horse powers.
	 * @param acc - Car's acceleration.
	 */
	public Wolkswagen(String mod, int hp, float acc) {
		model = mod;
		horsePower = hp;
		accelerationTime = acc;
	}

	/**
	 * Print attributes of a car.
	 */
	@Override
	public String toString() {
		return "This car is " + Wolkswagen.class.getSimpleName() + " with " + horsePower + " horsepower "
				+ "and acceleration from 0 to 60 miles/h for " + accelerationTime + "s.";
	}
}
