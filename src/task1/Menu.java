package task1;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * 
 * @author Velichko Spasov
 *
 */

public class Menu {
	public static List<Car> allCars = new ArrayList<>();

	/**
	 * Start point of this app
	 * Menu.
	 */
	public static void MainMenu() {
		int choice = 0;
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println("Main menu");
			System.out.println("1. Create new car object.");
			System.out.println("2. Print all created cars.");
			System.out.println("0. Exit");

			choice = scanner.nextInt();

			switch (choice) {
			case 1: {
				int carChoice = 0;

				do {
					System.out.println("Choose which car you want.");
					System.out.println("1. Audi.");
					System.out.println("2. BMW.");
					System.out.println("3. Wolkswagen.");
					System.out.println("0. Return.");

					carChoice = scanner.nextInt();

					switch (carChoice) {
					case 1: {
						scanner.nextLine();
						System.out.println("Audi model: ");
						String model = scanner.nextLine();

						System.out.println("Horse powers:");
						int hp = scanner.nextInt();

						System.out.println("Acceleration time:");
						float accTime = scanner.nextFloat();

						CreateCar(Audi.class.getName(), model, hp, accTime);

						break;
					}

					case 2: {
						scanner.nextLine();
						System.out.println("BMW model: ");
						String model = scanner.nextLine();

						System.out.println("Horse powers:");
						int hp = scanner.nextInt();

						System.out.println("Acceleration time:");
						float accTime = scanner.nextFloat();

						CreateCar(BMW.class.getName(), model, hp, accTime);

						break;
					}

					case 3: {
						scanner.nextLine();
						System.out.println("Wolkswagen model: ");
						String model = scanner.next();

						System.out.println("Horse powers:");
						int hp = scanner.nextInt();

						System.out.println("Acceleration time:");
						float accTime = scanner.nextFloat();

						CreateCar(Wolkswagen.class.getName(), model, hp, accTime);

						break;
					}
					}

				} while (carChoice != 0);

				break;
			}

			case 2: {
				PrintCars();

				break;
			}
			}
		} while (choice != 0);

		scanner.close();
	}

	/**
	 * @param className - Name of the class which we want to create a car.
	 * @param model - Model of the car.
	 * @param hp - Horse powers of that car.
	 * @param accTime - Acceleration time.
	 */
	public static void CreateCar(String className, String model, int hp, float accTime) {
		Object object = null;
		Constructor<?> ctor = null;
		Class<?> c = null;

		try {
			c = Class.forName(className);
		} catch (ClassNotFoundException e1) {
			System.out.println("No such class!");
			e1.printStackTrace();
		}

		try {
			ctor = c.getConstructor(String.class, int.class, float.class);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		try {
			object = ctor.newInstance(new Object[] { model, hp, accTime });
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		allCars.add((Car) object);
	}

	/**
	 * Print all cars.
	 */
	public static void PrintCars() {
		if (allCars.size() != 0) {
			for (Car car : allCars) {
				System.out.println(car.toString());
			}
		}
		else {
			System.out.println("There are not cars!");
		}
	}
}
