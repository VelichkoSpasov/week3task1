package task1;

/**
 * 
 * @author Velichko Spasov
 *
 */
public class BMW extends Car{
	public String model;
	public int horsePower;
	public float accelerationTime;
	
	/**
	 * 
	 * @param mod - Car's model.
	 * @param hp - Car's horse powers.
	 * @param acc - Car's acceleration.
	 */
	public BMW(String mod, int hp, float acc) {
		model = mod;
		horsePower = hp;
		accelerationTime = acc;
	}

	/**
	 * Print all attributes of a car.
	 */
	@Override
	public String toString() {
		return "This car is " + BMW.class.getSimpleName() + " with " + horsePower + " horsepower "
				+ "and acceleration from 0 to 60 miles/h for " + accelerationTime + "s.";
	}	
}
