package task2;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author Velichko SPasov	
 *
 */

public class Date{
	private String currentTime;
	private String currentDate;
	private String event;
	public static String regexForDate = "\\d{4}-\\d{2}-\\d{2}";
	public static String regexForTime = "\\d\\d:\\d\\d:\\d\\d";
	public static List<Date> dateList = new ArrayList<>();
	
	/**
	 * 
	 * @param date - the input date
	 * @param time - the input time
	 * @param eve - event
	 */
	public Date(String date, String time, String eve) {
		currentDate = date;
		currentTime = time;
		event = eve;
		dateList.add(this);
	}

	/**
	 * Print all events
	 */
	public static void PrintEvents() {
		for(Date date : dateList) {
			System.out.println(date.currentDate + " from " + date.currentTime + " - " + date.event);
		}
	}
}
