package task2;

import java.time.LocalDateTime;
import java.util.Scanner;

/**
 * Menu - start point
 *
 * @author Velichko Spasov
 */

public class Menu {
	static String date = "";
	static String time = "";
	static String event = "";
	static final int FIRST = 0;
	static final int SECOND = 1;
	static final int THIRD = 2;
	static LocalDateTime ldt = LocalDateTime.now();
	
	public static void StartMenu() {
		int choice = 0;
		boolean isCorrect = false;
		String localDate = ldt.toString();
		Scanner scanner = new Scanner(System.in);

		do {
			System.out.println("Menu");
			System.out.println("1. Add new date and event.");
			System.out.println("2. See all events.");
			System.out.println("0. Exit.");

			choice = scanner.nextInt();

			switch (choice) {
			case 1: {
				scanner.nextLine();

				while (!isCorrect) {
					System.out.println("Input date in format YYYY-MM-DD");
					date = scanner.nextLine();
					
					isCorrect = CheckFormatDate(localDate);
				}
				
				isCorrect = false;

				while (!isCorrect) {
					System.out.println("Input time in format HH:MM:SS");
					time = scanner.nextLine();

					isCorrect = CheckFormatTime();
				}

				while (event.isEmpty()) {
					System.out.println("Input event");
					event = scanner.nextLine();
				}
				
				new Date(date, time, event);

				break;
			}

			case 2: {
				Date.PrintEvents();

				break;
			}
			}

		} while (choice != 0);
		
		scanner.close();
	}

	/**
	 * Checks the input time. Return true only if everithing is correct.
	 * @return boolean for correct input or not.
	 */
	public static boolean CheckFormatTime() {
		String[] strings = time.split(":");
		
		try {
			/**
			 * Checks hours
			 */
			if (Parser(strings[FIRST]) < 1 || Parser(strings[FIRST]) > 24) {
				System.out.println("Hours can be from 1 to 24");
				
				return DefaultValue();
			}

			/**
			 * Checks minutes
			 */
			if (Parser(strings[SECOND]) < 0 || Parser(strings[SECOND]) > 59) {
				System.out.println("Minutes can be from 0 to 59");
				
				return DefaultValue();
			}
			
			/**
			 * Checks seconds
			 */
			if (Parser(strings[THIRD]) < 0 || Parser(strings[THIRD]) > 59) {
				System.out.println("Seconds can be from 0 to 59");

				return DefaultValue();
			}
		} catch (NumberFormatException e) {
			System.out.println("Only numbers for time!");

			return DefaultValue();
		}

		return true;
	}
	
	/**
	 * @return Sum of the given values
	 */
	public static int Sum(int a, int b) {
		return a + b;
	}
	
	
	/**
	 * @param value - value to be parsed to int
	 * @return	parsed int value
	 */
	public static int Parser(String value) {
		return Integer.parseInt(value);
	}
	
	/**
	 * 
	 * @return boolean false
	 */
	public static boolean DefaultValue() {
		time = "0";
		
		return false;
	}
	
	/**
	 * 
	 * @param localDate - current date
	 * @return boolean which means if the input values are allowed
	 */
	public static boolean CheckFormatDate(String localDate) {
		String[] strings = date.split("-");
		
		/*
		 * Checks if the value matches our regex
		 */
		if(!date.matches(Date.regexForDate))
			return DefaultValue();

		
		try {
			/**
			 * Checks the year
			 */
			if (Parser(strings[0]) < Parser(localDate.substring(0, 4)) ||
					(Parser(strings[0]) > (Parser(localDate.substring(0, 4) + 99)))) {
				System.out.println("Year bounds are: " + localDate.substring(0,  4) + " - " 
					+  Sum(Parser(localDate.substring(0, 4)), 99) + "."
					+ " You've entered: " + Parser(strings[0]) + ".");
				
				return DefaultValue();
			}
			/**
			 * Checks the month
			 */
			else if(Parser(strings[0]) == Parser(localDate.substring(0, 4))) {
				if((Parser(strings[1]) < Parser(localDate.substring(5, 7)))){
					System.out.println("Month can't be in the past! You entered: " + Parser(strings[1]) +
							". Current month is: " + Parser(localDate.substring(5, 7)) + ".");
					
					return DefaultValue();
				}
			}
			/**
			 * Again checks the month
			 */
			else if(Parser(strings[1]) < 1 || Parser(strings[1]) > 12) {
				System.out.println("Months are from 1 to 12");
				
				return DefaultValue();
			}
			/**
			 * Checks the day
			 */
			else if(Parser(strings[1]) % 2 == 0) {
				if(Parser(strings[1]) == 2 || Parser(strings[1]) == 02) {
					if(Parser(strings[0]) % 4 == 0) {
						/**
						 * Checks days for februray if its leap-year
						 */
						if(Parser(strings[2]) < 1 || Parser(strings[2]) > 29) {
							System.out.println("February has max 29 days this year.");
							
							return DefaultValue();
						}
					}
					else {
						/*
						 * Checks days for february if its not leap year
						 */
						if(Parser(strings[2]) < 1 || Parser(strings[2]) > 28) {
							System.out.println("February has max 28 days this year.");
							
							return DefaultValue();
						}
					}
				}
				/*
				 * Checks for every other month which has 30 days
				 */
				else {
					if(Parser(strings[2]) < 1 || Parser(strings[2]) > 30) {
						System.out.println("This month has 30 days");
						
						return DefaultValue();
					}
				}
			}
			else {
				/*
				 * Checks for every other month which has 31 days
				 */
				if(Parser(strings[2]) < 1 || Parser(strings[2]) > 31) {
					System.out.println("This month has 31 days");
					
					return DefaultValue();
				}
			}
		} catch (NumberFormatException e) {
			System.out.println("Only numbers for time!");

			return DefaultValue();
		}
		catch (IndexOutOfBoundsException e) {
			System.out.println("Index is out of bounds!");
		}

		return true;
	}
}
