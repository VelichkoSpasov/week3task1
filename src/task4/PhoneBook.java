package task4;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author Velichko Spasov
 *
 */
public class PhoneBook{
	public static Map<String, PhoneBook> phoneBook = new HashMap<>();
	private String firstName;
	private String middleName;
	private String lastName;
	
	/**
	 * 
	 * @param firName - first name
	 * @param midName - middle name
	 * @param lasName - last name
	 */
	public PhoneBook(String firName, String midName, String lasName) {
		firstName = firName;
		middleName = midName;
		lastName = lasName;
	}
	/**
	 * First check if this number already exists.
	 * @param firName - first name of the object
	 * @param midName - middle name of the object
	 * @param lasName - last name of the object
	 * @param num - number of the object
	 */
	public static void Insert(String firName, String midName, String lasName, String num) {
		if(phoneBook.containsKey(num)) {
			System.out.println("Number " + num + " already exists!");
			
			return;
		}
		
		phoneBook.put(num, new PhoneBook(firName, midName, lasName));
	}

	
	/**
	 * 
	 * @param num - number to update
	 */
	public static void Update(String num) {
		if(!Delete(num))
			return;
		
		Menu.Input();
	}

	/**
	 * Check if that number exists then remove it 
	 * @param num - number to delete
	 * @return - boolean false if that number doesn't exist
	 */
	public static boolean Delete(String num) {
		if(!phoneBook.containsKey(num)) {
			System.out.println("Number " + num + " does not exists!");
			
			return false;
		}
		
		phoneBook.remove(num);
		
		return true;
	}

	/**
	 * Print the phonebook
	 */
	public static void Print() {
		for (Map.Entry<String, PhoneBook> mapEle : phoneBook.entrySet()) {
			System.out.println(mapEle.getKey() + " " + mapEle.getValue().firstName + " "
					+ mapEle.getValue().middleName + " "
					+ mapEle.getValue().lastName);
		}
	}
}
