package task4;

import java.util.Scanner;
/**
 * 
 * @author Velichko Spasov
 *
 */
public class Menu {
	static Scanner scanner = new Scanner(System.in);
	static String nameRegex = "([A-Z][a-z]+)";

	/**
	 * Menu with options
	 */
	public static void MainMenu() {
		int choice = 999;

		do {
			System.out.println("Menu");
			System.out.println("1. Insert number.");
			System.out.println("2. Update number.");
			System.out.println("3. Delete number.");
			System.out.println("4. Print all numbers.");
			System.out.println("0. Exit");

			choice = scanner.nextInt();

			switch (choice) {
			case 1: {
				scanner.nextLine();
				Menu.Input();

				break;
			}

			case 2: {
				if(PhoneBook.phoneBook.isEmpty()) {
					System.out.println("Phonebook is empty!");
					
					return;
				}
				
				scanner.nextLine();

				System.out.println("Input number for update");
				String updateNum = scanner.nextLine();

				PhoneBook.Update(updateNum);

				break;
			}

			case 3: {
				if(PhoneBook.phoneBook.isEmpty()) {
					System.out.println("Phonebook is empty!");
					
					break;
				}
				
				scanner.nextLine();

				System.out.println("Input number to delete");
				String deleteNum = scanner.nextLine();

				PhoneBook.Delete(deleteNum);

				break;
			}

			case 4: {
				if(PhoneBook.phoneBook.isEmpty())
					System.out.println("Phonebook is empty!");
				else
					PhoneBook.Print();

				break;
			}
			}

		} while (choice != 0);

		scanner.close();
	}

	/**
	 * Input names and number
	 */
	public static void Input() {
		boolean flag = false;
		String fName = "";
		String mName = "";
		String lName = "";
		String num = "";

		do {
			if (fName == "") {
				System.out.println("Input first name.");
				fName = scanner.nextLine();

				if (!CheckNames(fName)) {
					fName = "";

					continue;
				}
			}

			if (mName == "") {
				System.out.println("Input middle name.");
				mName = scanner.nextLine();

				if (!CheckNames(mName)) {
					mName = "";

					continue;
				}
			}

			if (lName == "") {
				System.out.println("Input last name.");
				lName = scanner.nextLine();

				if (!CheckNames(lName)) {
					lName = "";

					continue;
				}
			}
			
			System.out.println("Input phone number.");
			num = scanner.nextLine();
			
			if(num.length() != 10) {
				System.out.println("Every number has 10 digits!");
				continue;
			}
			
			flag = true;
		} while (!flag);

		

		PhoneBook.Insert(fName, mName, lName, num);
	}

	/**
	 * 
	 * @param name - Check if it mathes the regex.
	 * @return - boolean true if match else false
	 */
	public static boolean CheckNames(String name) {
		if (name.matches(nameRegex))
			return true;
		else {
			System.out.println("Format is Aaa..");

			return false;
		}
	}
}
